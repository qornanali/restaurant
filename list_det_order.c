#include "header.h"

P_DetOrder allocDetOrder(){
	P_DetOrder P = (P_DetOrder) malloc(sizeof(O_DetOrder));
	Note(P) = newString(60);
	Next(P)  = NULL;
	return P;
}

void DeAllocDetOrder (P_DetOrder P){
	if (P != NULL){
		free (P);
	}
}

int getlastIdDetOrder(){
	
	int id_DetOrder;
	String S = newString(1000);
	S[0]=NULL;
	char tmp;
	int count=0;
	
	FILE *fptr;
	if ((fptr=fopen("t_DetOrder.txt","r"))==NULL){
		 printf("Tidak bisa membuka t_DetOrder.txt!");
		 return NULL;
	}else{
		int j=0;
		do {
			tmp = fgetc(fptr);
			if(tmp==';'){
				if(j==0){
					id_DetOrder = atoi(S);
				}
				j++;
				S[0]=NULL;		   
			}
			else if (tmp=='|'){
				count++;
				j=0;
				S[0]=NULL;
			}else{
				snprintf(S,1000,"%s%c",S,tmp);
			}
		}while(!feof(fptr));
		fclose(fptr);
		return id_DetOrder;
	}
}

void addDetOrder(){
	s("cls");
	
	O_DetOrder DetOrder;
	DetOrder.note = newString(60);
	printf("Id Order : ");
	scanf("%d",&DetOrder.id_order);
	printf("Id Menu : ");
	scanf("%d",&DetOrder.id_menu);
	printf("Jumlah : ");
	scanf("%d",&DetOrder.jumlah);
	printf("Note : ");
	fflush(stdin);
	fgets(DetOrder.note,60,stdin);
	DetOrder.id_detorder = getlastIdDetOrder()+1;
	insertDetOrder(DetOrder);
}

void selectDetOrder(){
	s("cls");
	L_DetOrder list_DetOrder;
	printf("Jumlah Data = %d\n\n",syncListDetOrder(&list_DetOrder));
	
	P_DetOrder P = First(list_DetOrder);
	while(P != NULL){
		printf("Id DetOrder : %d\nId Order : %d\nId Menu : %d\nJumlah : %d\nNote : %s\n\n",
			IdDetOrder(P),IdOrder(P),IdMenu(P),Jumlah(P),Note(P));
		P = Next(P);
	}
}

void printListDetOrdertoFile(L_DetOrder L){
	P_DetOrder P = First(L);	
	String data = newString(1000);
//	data[0] = NULL;
	FILE *fptr;
	if ((fptr=fopen("t_DetOrder.txt","r"))==NULL){
 		printf("Tidak bisa membuka t_DetOrder.txt!");
	}else{
		fclose(fptr);
		fptr=fopen("t_DetOrder.txt","w");
		while(P != NULL){
			fprintf(fptr, "%d;%d;%d;%d;%s;|\n",
			IdDetOrder(P),IdOrder(P),IdMenu(P),Jumlah(P),Note(P));
			P = Next(P);
		}
		fclose(fptr);
	}
}

P_DetOrder searchDetOrder(L_DetOrder L, int id_DetOrder){
	P_DetOrder P = First(L);
 	int found = 0;
 	while ((P != NULL) && (found == 0)){
 		if(IdDetOrder(P) == id_DetOrder){
 			found = 1;
		}else{
			P = Next(P);
		}
 	}
 	return (P);
}

void removeDetOrder(){
	s("cls");
	int id_DetOrder;
	printf("Id DetOrder : ");
	scanf("%d",&id_DetOrder);
	L_DetOrder list;
	syncListDetOrder(&list);
	deleteDetOrder(&list,id_DetOrder);
	printListDetOrdertoFile(list);
}

void updateDetOrder(){
	s("cls");
	int id_DetOrder;
	printf("Id DetOrder : ");
	scanf("%d",&id_DetOrder);
	L_DetOrder L;
	syncListDetOrder(&L);
	P_DetOrder P = searchDetOrder(L,id_DetOrder);
	if(P == NULL){
		printf("Tidak ditemukan Data dengan Id DetOrder : %d",id_DetOrder);
	}else{
		printf("%d\nId Order : ",IdOrder(P));
		scanf("%d",&IdOrder(P));
		printf("%d\nId Menu : ",IdMenu(P));
		scanf("%d",&IdMenu(P));
		printf("%d\nJumlah : ",Jumlah(P));
		scanf("%d",&Jumlah(P));
		printf("%s\nNote : ",Note(P));
		fflush(stdin);
		fgets(Note(P),60,stdin);
		printListDetOrdertoFile(L);
	}
}

void deleteDetOrder(L_DetOrder * L, int id_DetOrder){
	P_DetOrder PDel = First(*L);
	P_DetOrder Prec = NULL;
	int found = 0;
	while(PDel != NULL && found == 0){
		if(IdDetOrder(PDel) == id_DetOrder){
			found = 1;
		}else{
			Prec = PDel;
			PDel = Next(PDel);	
		}
	}
	if(found == 1){
		if (Prec == NULL && Next(PDel) == NULL){ 
			First(*L) = NULL; 
		}else 
		if (Prec == NULL){ 
			First(*L) = Next(PDel); 
		}else{ 
			Next(Prec) = Next(PDel); 
		}
 		Next(PDel) = NULL;
 		DeAllocDetOrder(PDel);
	}else{
		printf("Tidak ditemukan Data dengan Id DetOrder : %d",id_DetOrder);
	}	
}

void insertDetOrder(O_DetOrder X){
 	String data = newString(1000);
 	snprintf(data, sizeof(char)*1000, 
	 "%d;%d;%d;%d;%s;|\n",
			X.id_detorder,X.id_order,X.id_menu,X.jumlah,X.note);
	FILE *fptr;
 	fptr=fopen("t_DetOrder.txt", "a");
 	if(fptr==NULL){
 		printf("Tidak bisa membuka t_DetOrder.txt!");
 	}else{
		fprintf(fptr, "%s", data);
		fclose(fptr);
	}
}

void createListDetOrder(L_DetOrder * L){
	First((*L)) = NULL;
}

int syncListDetOrder(L_DetOrder * L){
	
	createListDetOrder(L);
	
	P_DetOrder P,PData;
	PData = First((*L));
	String S = newString(1000);
	S[0]=NULL;
	char tmp;
	int count=0;
	
	FILE *fptr;
	if ((fptr=fopen("t_DetOrder.txt","r"))==NULL){
		 printf("Tidak bisa membuka t_DetOrder.txt!");
	}else{
		P = allocDetOrder();
		int j=0;
		do {
			tmp = fgetc(fptr);
			if(tmp==';'){
				switch(j){
					case 0 :
						IdDetOrder(P) = atoi(S);
						break;
					case 1 :
						IdOrder(P) = atoi(S);
						break;
					case 2 :
						IdMenu(P) = atoi(S);
						break;
					case 3 :
						Jumlah(P) = atoi(S);
						break;
					case 4 :
						strcpy(Note(P),S);
						break;
				}
			j++;
			S[0]=NULL;		   
			}
			else if (tmp=='|'){
				count++;
				j=0;
				if(PData==NULL){
					First((*L)) = P;
					PData = P;
				}
				else{
					Next(PData) = P;
					PData = Next(PData);
				}
				P = allocDetOrder();
				S[0]=NULL;
			}else{
				snprintf(S,1000,"%s%c",S,tmp);
			}
		}while(!feof(fptr));
		P_DetOrder tmp1 = First((*L));
		fclose(fptr);
	}
	return count;
}
