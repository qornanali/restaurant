#include <stdio.h>
#include <limits.h>
#include <malloc.h>
#include <windows.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>

#define Next(P) (P)->next
#define First(L) (L).first

#define NamaPengunjung(P) (P)->nama_pengunjung
#define Date(P) (P)->tgl_order.date
#define Month(P) (P)->tgl_order.month
#define Year(P) (P)->tgl_order.year
#define Hour(P) (P)->tgl_order.hour
#define Min(P) (P)->tgl_order.min
#define IdOrder(P) (P)->id_order
#define IdBooking(P) (P)->id_booking

#define IdDetOrder(P) (P)->id_detorder
#define IdMenu(P) (P)->id_menu
#define Jumlah(P) (P)->jumlah
#define Note(P) (P)->note

#ifndef header_H

//string
typedef char * String;

//date
typedef struct {
 	int date;
 	int month;
 	int year;
 	int hour;
 	int min;
} O_Date;

//t_order
typedef struct O_Order * P_Order;
typedef struct O_Order{
 	int id_order;
	O_Date tgl_order;
	int id_booking;
	String nama_pengunjung;
	P_Order next;
} O_Order;
typedef struct{
	P_Order first;
} L_Order;

//list_order.c
P_Order allocOrder();
void DeAllocOrder (P_Order P);
P_Order searchOrder(L_Order L, int id_order);
void insertOrder(O_Order X);
void updateOrder();
void deleteOrder(L_Order * L, int id_order);
void insertOrder(O_Order X);
void selectOrder();
void addOrder();
void removeOrder();
void createListOrder(L_Order * L);
int syncListOrder(L_Order * L);
void printListOrdertoFile(L_Order L);
int getlastIdOrder();

//t_det_order
typedef struct O_DetOrder * P_DetOrder;
typedef struct O_DetOrder{
	int id_detorder;
 	int id_order;
 	int id_menu;
 	int jumlah;
 	String note;
	P_DetOrder next;
} O_DetOrder;
typedef struct{
	P_DetOrder first;
} L_DetOrder;

//list_det_order.c
P_DetOrder allocDetOrder();
void DeAllocDetOrder (P_DetOrder P);
P_DetOrder searchDetOrder(L_DetOrder L, int id_DetOrder);
void insertDetOrder(O_DetOrder X);
void updateDetOrder();
void deleteDetOrder(L_DetOrder * L, int id_DetOrder);
void insertDetOrder(O_DetOrder X);
void selectDetOrder();
void addDetOrder();
void removeDetOrder();
void createListDetOrder(L_DetOrder * L);
int syncListDetOrder(L_DetOrder * L);
void printListDetOrdertoFile(L_DetOrder L);
int getlastIdDetOrder();

//file.c
char * newString(int size);
void rewritefile(int id_file);
String readFile(int id_file);
void s(String c);

#endif
