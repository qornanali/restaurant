#include "header.h"

P_Order allocOrder(){
	P_Order P = (P_Order) malloc(sizeof(O_Order));
	NamaPengunjung(P) = newString(60);
	Next(P)  = NULL;
	return P;
}

void DeAllocOrder (P_Order P){
	if (P != NULL){
		free (P);
	}
}

int getlastIdOrder(){
	
	int id_order;
	String S = newString(1000);
	S[0]=NULL;
	char tmp;
	int count=0;
	
	FILE *fptr;
	if ((fptr=fopen("t_order.txt","r"))==NULL){
		 printf("Tidak bisa membuka t_order.txt!");
		 return NULL;
	}else{
		int j=0;
		do {
			tmp = fgetc(fptr);
			if(tmp==';'){
				if(j==0){
					id_order = atoi(S);
				}
				j++;
				S[0]=NULL;		   
			}
			else if (tmp=='|'){
				count++;
				j=0;
				S[0]=NULL;
			}else{
				snprintf(S,1000,"%s%c",S,tmp);
			}
		}while(!feof(fptr));
		fclose(fptr);
		return id_order;
	}
}

void addOrder(){
	s("cls");
	
	O_Order order;
	order.nama_pengunjung = newString(60);
	printf("Nama Pengunjung : ");
//	scanf("%s",);
	fgets(order.nama_pengunjung,60,stdin);
	printf("Tanggal Order (dd/MM/yyyy hh:mm) : ");
	scanf("%d/%d/%d %d:%d",&order.tgl_order.date,&order.tgl_order.month,&order.tgl_order.year,&order.tgl_order.hour,&order.tgl_order.min);
	printf("Id Booking : ");
	scanf("%d",&order.id_booking);
	order.id_order = getlastIdOrder()+1;
	insertOrder(order);
}

void selectOrder(){
	s("cls");
	L_Order list_order;
	printf("Jumlah Data = %d\n\n",syncListOrder(&list_order));
	
	P_Order P = First(list_order);
	while(P != NULL){
		printf("Id Order : %d\nId Booking : %d\nTanggal Order : %d/%d/%d %d:%d\nNama Pengunjung : %s\n\n",
			IdOrder(P),IdBooking(P),Date(P),Month(P),Year(P),Hour(P),Min(P),NamaPengunjung(P));
		P = Next(P);
	}
}

void printListOrdertoFile(L_Order L){
	P_Order P = First(L);	
	String data = newString(1000);
//	data[0] = NULL;
	FILE *fptr;
	if ((fptr=fopen("t_order.txt","r"))==NULL){
 		printf("Tidak bisa membuka t_order.txt!");
	}else{
		fclose(fptr);
		fptr=fopen("t_order.txt","w");
		while(P != NULL){
			fprintf(fptr, "%d;%d;%d;%d;%d;%d;%d;%s;|\n", 
			IdOrder(P),Date(P),Month(P),Year(P),Hour(P),Min(P),IdBooking(P),NamaPengunjung(P));
//				printf("%d;%d;%d;%d;%d;%d;%d;%s;|\n",
//				IdOrder(P),Date(P),Month(P),Year(P),Hour(P),Min(P),IdBooking(P),NamaPengunjung(P));
			P = Next(P);
		}
		fclose(fptr);
	}
}

P_Order searchOrder(L_Order L, int id_order){
	P_Order P = First(L);
 	int found = 0;
 	while ((P != NULL) && (found == 0)){
 		if(IdOrder(P) == id_order){
 			found = 1;
		}else{
			P = Next(P);
		}
 	}
 	return (P);
}

void removeOrder(){
	s("cls");
	int id_order;
	printf("Id Order : ");
	scanf("%d",&id_order);
	L_Order list;
	syncListOrder(&list);
	deleteOrder(&list,id_order);
	printListOrdertoFile(list);
}

void updateOrder(){
	s("cls");
	int id_order;
	printf("Id Order : ");
	scanf("%d",&id_order);
	L_Order L;
	syncListOrder(&L);
	P_Order P = searchOrder(L,id_order);
	if(P == NULL){
		printf("Tidak ditemukan Data dengan Id Order : %d",id_order);
	}else{
//		scanf("%s",NamaPengunjung(P));
		printf("Nama Pengunjung : %s\nBaru : ",NamaPengunjung(P));
		fflush(stdin);
		fgets(NamaPengunjung(P),60,stdin);
		printf("Tanggal Order : %d/%d/%d %d:%d\nBaru (dd/MM/yyyy hh:mm) : ",Date(P),Month(P),Year(P),Hour(P),Min(P));
		scanf("%d/%d/%d %d:%d",&Date(P),&Month(P),&Year(P),&Hour(P),&Min(P));
		printf("Id Booking : %d\nBaru : ",IdBooking(P));
		scanf("%d",&IdBooking(P));
		printListOrdertoFile(L);
	}
}

void deleteOrder(L_Order * L, int id_order){
	P_Order PDel = First(*L);
	P_Order Prec = NULL;
	int found = 0;
	while(PDel != NULL && found == 0){
		if(IdOrder(PDel) == id_order){
			found = 1;
		}else{
			Prec = PDel;
			PDel = Next(PDel);	
		}
	}
	if(found == 1){
		if (Prec == NULL && Next(PDel) == NULL){ 
			First(*L) = NULL; 
		}else 
		if (Prec == NULL){ 
			First(*L) = Next(PDel); 
		}else{ 
			Next(Prec) = Next(PDel); 
		}
 		Next(PDel) = NULL;
 		DeAllocOrder(PDel);
	}else{
		printf("Tidak ditemukan Data dengan Id Order : %d",id_order);
	}	
}

void insertOrder(O_Order X){
 	String data = newString(1000);
 	snprintf(data, sizeof(char)*1000, "%d;%d;%d;%d;%d;%d;%d;%s;|\n",X.id_order,X.tgl_order.date,X.tgl_order.month,X.tgl_order.year,X.tgl_order.hour,X.tgl_order.min,X.id_booking,X.nama_pengunjung);
	FILE *fptr;
 	fptr=fopen("t_order.txt", "a");
 	if(fptr==NULL){
 		printf("Tidak bisa membuka t_order.txt!");
 	}else{
		fprintf(fptr, "%s", data);
		fclose(fptr);
	}
}

void createListOrder(L_Order * L){
	First((*L)) = NULL;
}

int syncListOrder(L_Order * L){
	
	createListOrder(L);
	
	P_Order P,PData;
	PData = First((*L));
	String S = newString(1000);
	S[0]=NULL;
	char tmp;
	int count=0;
	
	FILE *fptr;
	if ((fptr=fopen("t_order.txt","r"))==NULL){
		 printf("Tidak bisa membuka t_order.txt!");
	}else{
		P = allocOrder();
		int j=0;
		do {
			tmp = fgetc(fptr);
			if(tmp==';'){
				switch(j){
					case 0 :
						IdOrder(P) = atoi(S);
						break;
					case 1 :
						Date(P) = atoi(S);
						break;
					case 2 :
						Month(P) = atoi(S);
						break;
					case 3 :
						Year(P) = atoi(S);
						break;
					case 4 :
						Hour(P) = atoi(S);
						break;
					case 5 :
						Min(P) = atoi(S);
						break;
					case 6 :
						IdBooking(P) = atoi(S);
						break;
					case 7 :
						strcpy(NamaPengunjung(P),S);
						break;
				}
			j++;
			S[0]=NULL;		   
			}
			else if (tmp=='|'){
				count++;
				j=0;
				if(PData==NULL){
					First((*L)) = P;
					PData = P;
				}
				else{
					Next(PData) = P;
					PData = Next(PData);
				}
				P = allocOrder();
				S[0]=NULL;
			}else{
				snprintf(S,1000,"%s%c",S,tmp);
			}
		}while(!feof(fptr));
		P_Order tmp1 = First((*L));
		fclose(fptr);
	}
	return count;
}
